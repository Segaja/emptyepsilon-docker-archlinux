# EmptyEpislon docker image based on archlinux

This repository provides a docker image based on the [archlinux](https://www.archlinux.org/) [archlinux/base](https://hub.docker.com/r/archlinux/base) docker image that provides a means to play [EmptyEpsilon](http://daid.github.io/EmptyEpsilon/).

The following packages are being installed:
- [pulseaudio](https://www.archlinux.org/packages/extra/x86_64/pulseaudio/)
- [emptyepsilon](https://www.archlinux.org/packages/community/x86_64/emptyepsilon/)

## Usage
```bash
./run.sh
```
This should build the `emptyepsilon` image and run it.

## Prerequesits
This setup depends on a few systems running on your local machine:
- docker
- pulseaudio
- an X server
