FROM archlinux/base

RUN pacman -Syu --noconfirm emptyepsilon pulseaudio \
    && echo -e "default-server = unix:/run/user/1000/pulse/native\nautospawn = no\ndaemon-binary = /bin/true\nenable-shm = false" > /etc/pulse/client.conf \
    && useradd -u 1000 -G audio ee \
    && install -o ee -g ee -d ${HOME}/.config/pulse/

USER ee

RUN pulseaudio --daemonize=yes --start

ENV DISPLAY :0

ENTRYPOINT ["/usr/bin/EmptyEpsilon"]
CMD ["fullscreen=0"]
