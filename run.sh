#/usr/bin/env bash

IMAGENAME="emptyepsilon"

docker build -t "${IMAGENAME}" .
docker run --rm -ti --volume=${HOME}/.Xauthority:/home/ee/.Xauthority --volume=${XDG_RUNTIME_DIR}/pulse:/run/user/1000/pulse --network=host -e DISPLAY=${DISPLAY} "${IMAGENAME}"
